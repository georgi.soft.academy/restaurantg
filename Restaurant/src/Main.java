import com.restaurant.customer.Customer;
import com.restaurant.table.Bill;
import com.restaurant.table.MenuItem;
import com.restaurant.table.Table;
import com.restaurant.waiter.Waiter;
import java.io.IOException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {

        Customer restaurant = new Customer("27321789789", "Test Restaurant EOOD", "Plovdiv Bulgaria ul.'Georgi Carigradski 7'",
                "Soft Academy", "+359-99-99-99-99");

        Scanner sc = new Scanner(System.in);
        System.out.println("Please enter Your ID number");
        String idNumber = sc.nextLine();

        Waiter.checkWaiter(idNumber);

        Waiter assignedWaiterName = Waiter.Names(idNumber);

        System.out.println("Enter table number");
        int tableNumber = sc.nextInt();
        sc.nextLine();// Консумира на новия ред

        Table.checkTableExistance(tableNumber);

        Bill bill = new Bill(assignedWaiterName.getName(), tableNumber);

        //adding ordered items
        System.out.println("Enter ordered items (enter 'done' to finish)");
        String itemName;
        while (true){
            System.out.println("Item name: ");
            itemName = sc.nextLine();
            if(itemName.equals("done")){
                break;
            }
            System.out.println("Item price: ");
            double itemPrice = sc.nextDouble();
            sc.nextLine();//изчакваме да консумираме следващия ред
            sc.nextLine();//изчиства буфера на скенера

            MenuItem item = new MenuItem(itemName, itemPrice);
            bill.addOrderedItem(item);
        }

        System.out.println("Enter discount amount(optional): ");
        double discount = sc.nextDouble();
        bill.setDiscount(discount);

        System.out.println("#########################################");
        System.out.println("############ Restaurant #################");
        System.out.println("#########################################");
        System.out.println(restaurant.getAddress());
        System.out.println(restaurant.getName());
        System.out.println(restaurant.getEIK());
        bill.printBill();
        System.out.println("\nManager Name: " + restaurant.getManagerName());
        System.out.println("Manager Phone: " + restaurant.getManagerPhone());


    }
}


